import React from 'react';
import {Link} from 'react-router-dom'
import './cssComp.css'


const WelcomePageComp = (props) => {



    let goCalculate = () => {
        return props.history.push('/calculator')
    }

    return (
        <div className="wrapper" >
            <div className="navbar">
                <div style={{padding: "10px"}}>
                    <h1 style={{color: 'lightgrey', margin:"auto"}}> WORTHY</h1>
                    <Link to="/about">about us</Link>
                </div>

                <br/><br/><br/>

                <h1 className="h1"> welcome to Worthy! </h1>

                <div>
                    Have a diamond and dont know it's worth? <br/><br/><br/>
                    <input type="button" value="click here to find out" className="button1" onClick={goCalculate}/>
                </div>
            </div>
        </div>
    );
};

export default WelcomePageComp;