import React from 'react';
import {Switch, Route} from 'react-router-dom'
import WelcomePage from './WelcomePageComp'
import AboutComp from './AboutComp'
import CalculetorComp from './CalculetorComp';
import SimilarResultsComp from './SimilarResultsComp';

const HostComp = () => {
    return (
        <div>
            <Switch>
                <Route path="/" exact component={WelcomePage}/>
                <Route path="/calculator" component={CalculetorComp}/>
                <Route path="/about" component={AboutComp}/>
                <Route path="/similarResults" component={SimilarResultsComp}/>
            </Switch>

        </div>
    );
};

export default HostComp;