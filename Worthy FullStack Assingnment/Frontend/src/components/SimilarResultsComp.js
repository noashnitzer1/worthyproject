import React, { useEffect, useState } from 'react';
import axios from 'axios'
import LastComp from './LastComp'

const SimilarResultsComp = () => {
    const[showDiamonds, setShowDiamonds] = useState([])

    useEffect(async() => {
        let diamonds = await axios.get('http://localhost:8002/Diamonds')
        let items = diamonds.data.map((diamond, index) => {
            return(
                <LastComp key={index} data={diamond}/>
            )
        })
        setShowDiamonds(items)
    },[])


    return (
        <div>
            <br/>
            {showDiamonds}
            <br/>
        </div>
    );
};

export default SimilarResultsComp;