const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/worthyDB' , {
    useNewUrlParser : true,
    useUnifiedTopology : true
})