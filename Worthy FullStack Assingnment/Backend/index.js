let cors = require('cors')
let bodyParser = require('body-parser')
let express = require('express')
let worthyController = require('./controller//worthyController')
require('./config/worthyDB')


let app = express()
app.use(cors())
app.use(bodyParser.urlencoded({extended:true})).use(bodyParser.json())
app.use('/Diamonds', worthyController)


app.listen(8002, () => {
    console.log("server is up")
})