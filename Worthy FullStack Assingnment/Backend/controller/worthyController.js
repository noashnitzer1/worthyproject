const express = require('express')
const diamondBL = require('../models/worthyBL')
const router = express.Router()

//getAll
router.route('/').get(async(req ,resp) => {
    let diamonds = await diamondBL.getAllDiamonds()
    return resp.json(diamonds)
})


//getById
router.route('/:id').get(async(req, resp) => {
    let id = req.params.id
    let diamond = await diamondBL.getDiamondById(id)
    return resp.json(diamond)
})


//create
router.route('/').post(async(req, resp) => {
    let newDiamond = req.body
    let diamondToAdd = await diamondBL.addDiamond(newDiamond)
    return resp.json(diamondToAdd)
})


//update
router.route('/:id').put(async(req, resp) => {
    let id = req.params.id
    let newData = req.body
    let diamondToUpdate = await diamondBL.updateDiamond(id, newData)
    return resp.json(diamondToUpdate)
})


//delete
router.route('/:id').delete(async(req, resp) => {
    let id = req.params.id
    let diamondToDelete = await diamondBL.deleteDiamond(id)
    return resp.json(diamondToDelete)
})



module.exports = router