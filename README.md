# Worthy diamond's worth calculator - Full Stack Project

---

## General info:
This is a project aimed for a marketplacr of luxury goods. This project is focused on diamonds. Diamonds can be categorized by their Carat, Weigth, Color and Clarity. In this project i have built a feature that lets the user pass his diamond characteristics and returns the final price according to them.
---

## Technologies:
**This project is created with:**
- JavaScript
- Node.js
- MongoDB
- React
- CSS

---

## Setup
in order to run the project, run the command "node index.js" in the terminal of the server's index, and run the command "npm start" in the terminal of the app in the react frontend folder.

---

## Project explanation
This project is divided to 2 folders: Backend and Frontend.

**Backend**
folders:
- config - connection to project's DB
- controller - project's controller
- models - project's BL and Schems
- index.js

**Frontend**
folders:
- Images - images imported for website design
- components - all components of the project:
    - HostComp - defines the rouths for each component and determines the default first component to open.
    - WelcomePageComp - the website's HomePage. in which has a link to the 'AboutComp' and a button leading to the 'CalculatorComp'.
    - AboutComp - some information about the company and a link leading to 'CalculatorComp".
    - CalculatorComp - the component calculating the diamond's worth according to the user's input.
    - SimilarResultsComp - has a 'get' request in order to get the data from the Database. Leading to the 'LastComp'
    - LastComp - shows the diamond rings stored in the Database and their information(cut, clarity, color, carat)
    - cssComp - all css designes for the components
- app.js



