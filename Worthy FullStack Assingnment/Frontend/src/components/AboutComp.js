import React from 'react';
import ImgAbout from '../Images/imgAbout.jpg'
import {Link} from 'react-router-dom'
import './cssComp.css'


const AboutComp = () => {
    return (


        <div  >
            <div className="about">
                <h1 className="h"> WORTHY</h1>
                <h4 >ABOUT US</h4>
            </div>

            <br/><br/>
            <img src={ImgAbout}/>
            <br/><br/>

            <div style={{width:"650px", margin:"auto"}}>
                Worthy is dedicated to providing sellers and buyers with 
                that ultimate win-win. Everyday we bring together buyers and 
                sellers with our luxury auction marketplace. Worthy is led 
                by the very best in the luxury goods market and we work 
                together to bring you the fairest market value for your 
                valuable items while providing stellar service.

                <h4>“ All We Want From You Is To Sit Back, Relax, And Watch The Offers Roll In. ”</h4>

                We believe that you should get the most for your valuables 
                in as little time as possible, with as little effort as possible. 
                Whether you need financing to run your small business, to plan 
                for the future, or to go on a vacation, we are here for you. 
            </div>

            <br/><br/><br/>

            <div className="tocalculate">
                for diamond's worth calculation <Link to="/calculator">click here</Link>
            </div>
        </div>
    );
};

export default AboutComp;