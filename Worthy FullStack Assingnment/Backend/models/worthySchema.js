let mongoose = require('mongoose')
let schema = mongoose.Schema

let DiamondsSchema = new schema({
    Image: String,
    CaratWeight: Number,
    Cut: String,
    Color: String,
    Clarity: String
})

module.exports = mongoose.model('Diamonds', DiamondsSchema)