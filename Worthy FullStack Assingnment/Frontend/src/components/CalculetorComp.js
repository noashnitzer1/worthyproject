import React , {useState} from 'react';
import ImgClarity from '../Images/imgClarity.png'
import ImgCut from '../Images/imgCut.png'
import SimilarResultsComp from './SimilarResultsComp';


const CalculetorComp = () => {
    const [caratWeight, setCaratWeigth] = useState("")
    const [cut, setCut] = useState("ROUND")
    const [color, setColor] = useState("D")
    const [clarity, setClarity] = useState("IF")
    const [worth, setWorth] = useState()
    const [isHidden, setIsHidden] = useState(false)
    const [comp, setComp] = useState("")

    let calculateWorth =(e) => {
        e.preventDefault()
        let num1 = 0
        let num2 = 0
        let num3 = 0
        let num4 = 0

        num1 = parseInt(caratWeight.caratWeight) * 1000

        if(color == "D"){
            num2 = 2000
        }else if (color == "E"){
            num2 = 1850
        }else if (color == "F"){
            num2 = 1600
        }else if (color == "G"){
            num2 = 1520
        }else if (color == "H"){
            num2 = 1300
        }else if (color == "I"){
            num2 = 1270
        }else if (color == "J"){
            num2 = 1000
        }

        if(clarity == "FL"){
            num3 = 1000
        }else if (clarity == "IF"){
            num3 = 850
        }else if (clarity == "VVS1"){
            num3 = 700
        }else if (clarity == "VVS2"){
            num3 = 650
        }else if (clarity == "VS1"){
            num3 = 550
        }

        if(cut == "ROUND"){
            num4 = 2000
        }else if(cut == "PRINCESS"){
            num4 = 1500
        }else if(cut == "CUSHION"){
            num4 = 1850
        }else if(cut == "MARQUISE"){
            num4 = 1370
        }else if(cut == "EMERALD"){
            num4 = 1500
        }
        
        let sum = (num1+num2+num3+num4)
        setWorth(sum)
        setIsHidden(true)
    }


    let showSimilarResults = () => {
        setComp(<SimilarResultsComp/>)
    }


    return (
        <div className="about">
            <h1 className="h"> WORTHY</h1>
            <h3>let's calculate your diamond's worth!</h3>

            <form onSubmit={calculateWorth}>
                1. carat weigth <br/>
                <input type="number" onChange={e => setCaratWeigth({caratWeight:e.target.value})} />

                <br/><br/><br/>

                2. color <br/>
                <select style={{width: '173px'}} onChange={e => setColor(e.target.value)} name="color">
                    <option value="D">D</option>
                    <option value="E">E</option>
                    <option value="F">F</option>
                    <option value="G">G</option>
                    <option value="H">H</option>
                    <option value="I">I</option>
                    <option value="J">J</option>
                </select>

                <br/><br/><br/>

                3. cut <br/>
                <img style={{border: "solid 1px black"}} src={ImgCut}/><br/>
                <select style={{width: '173px'}} onChange={e => setCut(e.target.value)} name="cut">
                    <option value="ROUND">ROUND</option>
                    <option value="PRINCESS">PRINCESS</option>
                    <option value="CUSHION">CUSHION</option>
                    <option value="MARQUISE">MARQUISE</option>
                    <option value="EMERALD">EMERALD</option>
                </select>

                <br/><br/><br/>

                4. clarity <br/>
                <img style={{border: "solid 1px black"}} src={ImgClarity}/><br/>
                <select style={{width: '173px'}} onChange={e => setClarity(e.target.value)} name="clarity">
                    <option value="FL">FL</option>
                    <option value="IF">IF</option>
                    <option value="VVS1">VVS1</option>
                    <option value="VVS2">VVS2</option>
                    <option value="VS1">VS1</option>
                </select>

                <br/><br/>

                <input className="button1" type="submit" value="calculate worth"/>
            </form>

            {isHidden && <div> your diamond's worth is {worth} $ </div>}

            <br/><br/>

            <input className="button1" type="button" value="for similar results" onClick={showSimilarResults}/>
            {comp}


        </div>
    
    );
}



export default CalculetorComp;