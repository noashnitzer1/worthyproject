import React from 'react';

const LastComp = (props) => {

    return (
        <div>
            <br/>
            <img style={{height:"100px", width:"100px" }} src={props.data.Image}/><br/>
            Carat Weight: {props.data.CaratWeight}<br/>
            Cut: {props.data.Cut}<br/>
            Color: {props.data.Color}<br/>
            Clarity: {props.data.Clarity}<br/>
            <br/>
        </div>
    );
};

export default LastComp;