let diamond = require('./worthySchema')

//getAll
let getAllDiamonds = () => {
    return new Promise((resolve, reject) => {
        diamond.find({}, (err, data) => {
            if(err){
                reject(err)
            }else{
                resolve(data)
            }
        })
    })
}


//getById
let getDiamondById = () => {
    return new Promise((resolve, reject) => {
        diamond.findById(id, (err, data) => {
            if(err){
                reject(err)
            }else{
                resolve(data)
            }
        })
    })
}



//create
let addDiamond = () => {
    return new Promise((resolve, reject) => {
        let newDiamond = new diamond({
            caratWeight: diamond.caratWeight,
            cut: diamond.cut,
            color: diamond.color,
            clarity: diamond.clarity
        })
        newDiamond.save((err) => {
            if(err){
                reject(err)
            }else{
                resolve("new diamond has been added successfuly!")
            }
        })
    })
}


//update
let updateDiamond = () => {
    return new Promise((resolve, reject) => {
        diamond.findByIdAndUpdate(id, {
                caratWeight: diamond.caratWeight,
                cut: diamond.cut,
                color: diamond.color,
                clarity: diamond.clarity
            }, (err) => {
            if(err){
                reject(err)
            }else{
                resolve("diamond has been updated successfuly!")
            }
        })
    })
}


//delete
let deleteDiamond = () => {
    return new Promise((resolve, reject) => {
        diamond.findByIdAndDelete(id, (err) => {
            if(err){
                reject(err)
            }else{
                resolve("diamond deleted successfuly!")
            }
        })
    })
}



module.exports = {getAllDiamonds, getDiamondById, addDiamond, updateDiamond, deleteDiamond}